﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class ItemManager : MonoBehaviour
{
    public int maxItemAtScreen = 1;
    public GameObject[] listItem;
    public int numberItem;
    private GameObject objTemp;
    // Use this for initialization
    void Start()
    {
        //Move to top screen;
        transform.position = new Vector3(transform.position.x, Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height)).y + 1.5f, transform.position.z);
        StartCoroutine(UpdateMaxItemAtScreen());
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.GameState == GameState.Playing)
        {
            numberItem = GameObject.FindGameObjectsWithTag("Item").Length;
            if (numberItem < maxItemAtScreen)
            {
                int indexItem = Random.Range(0, listItem.Length);
                objTemp = Instantiate(listItem[indexItem], transform.position, Quaternion.identity);
                //float halfWidthCart = objTemp.GetComponent<Renderer>().bounds.size.x / 2;
                Vector3 temp = new Vector3(transform.position.x, objTemp.transform.position.y, objTemp.transform.position.z);
                temp.x = Random.Range(-Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height)).x, Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height)).x);
                //temp.x = Mathf.Clamp(objTemp.transform.position.x, -Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height)).x + halfWidthCart, Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height)).x - halfWidthCart);
                objTemp.transform.position = temp;
            }
        }
    }
    IEnumerator UpdateMaxItemAtScreen()
    {
        yield return new WaitForSeconds(30);
        if (maxItemAtScreen < 5)
        {
            ++maxItemAtScreen;
            StartCoroutine(UpdateMaxItemAtScreen());
        }

    }
    
}
