﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    static public UIManager Instance;
    public Text txtScore;
    public GameObject pnlGameOver;
    public Text txtPnlScore;
    public Text txtPnlScoreHight;
    private void Awake()
    {
        if (!Instance)
            Instance = this;
    }
    private void OnEnable()
    {
        CartController.PlayerDied += ShowGameOver;
    }
    private void OnDisable()
    {
        CartController.PlayerDied -= ShowGameOver;
    }
    public void ShowGameOver()
    {
        GameManager.Instance.GameState = GameState.GameOver;
        pnlGameOver.SetActive(true);
        GameManager.Instance.StopMusic();
        SoundManager.Instance.PlaySound(SoundManager.Instance.soundGameOver);

        txtPnlScore.text = ScoreManager.Instance.GetScore().ToString();
        txtPnlScoreHight.text = PlayerPrefs.GetInt("HIGHTSCORE").ToString();
    }
    public void HideGameOver()
    {
        GameManager.Instance.GameState = GameState.Playing;
        pnlGameOver.SetActive(false);
    }
    public void btnPlayAgain()
    {
#pragma warning disable CS0618 // Type or member is obsolete
        Application.LoadLevel(0);
#pragma warning restore CS0618 // Type or member is obsolete
    }
}
