﻿using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    static public ScoreManager Instance;
    private int score;
    private int hightScore;
    private void Awake()
    {
        if (!Instance)
            Instance = this;
        hightScore = PlayerPrefs.GetInt("HIGHTSCORE");
    }

    public int GetScore()
    {
        return score;
    }
    public void AddScore(int amou)
    {
        score += amou;
        if (score > hightScore)
        {
            hightScore = score;
            PlayerPrefs.SetInt("HIGHTSCORE", hightScore);
        }
        SoundManager.Instance.PlaySound(SoundManager.Instance.soundScore);
        // CHANGE TEXT SCORE
        UIManager.Instance.txtScore.text = score.ToString();
    }

}
