﻿using System.Collections;
using UnityEngine;

public class SquareController : MonoBehaviour
{

    public float speedMove = 10;
    public Color mColor;
    [HideInInspector] public bool allowMove = false;
    private AudioSource audioSource;

    public ParticleSystem particle;
    // Use this for initialization
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        int indexColor = Random.Range(0, ColorManager.Instance.listColor.Length);
        mColor = ColorManager.Instance.listColor[indexColor];
        transform.GetComponent<Renderer>().material.color = mColor;
        if (indexColor == 0)
            transform.tag = "Obstancle";
        particle = GameObject.FindGameObjectWithTag("BoomEvent").GetComponent<ParticleSystem>();
        StartCoroutine(checkAllow());
        StartCoroutine(FindBoomEvent());
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.GameState == GameState.Playing)
            if (allowMove)
                ControlMove();

    }
    void ControlMove()
    {
        transform.position += Vector3.down * speedMove * Time.deltaTime;
    }
    IEnumerator checkAllow()
    {
        yield return new WaitForSeconds(0f);
        allowMove = true;
    }
    IEnumerator FindBoomEvent()
    {
        yield return new WaitForSeconds(0.5f);

    }
    private void OnCollisionEnter2D(Collision2D obj)
    {
        // This tag is Item
        // When collised with object tag is Lad then change to Obstancle
        if (obj.transform.tag == "Land")
        {
            transform.tag = "Obstancle";
            transform.GetComponent<Renderer>().material.color = ColorManager.Instance.listColor[0];
            audioSource.Play();
            allowMove = false;
        }
        else
        {
            particle.transform.position = transform.position;
            particle.GetComponent<Renderer>().material.color = transform.GetComponent<Renderer>().material.color;
            particle.GetComponent<ParticleSystem>().Play();

            if (obj.transform.tag == "Obstancle" && obj.transform.GetComponent<SquareController>().allowMove == false)
            {
                audioSource.Play();
                Destroy(obj.gameObject, 0.2f);
                Destroy(gameObject, 0.2f);

            }
        }
    }


}
