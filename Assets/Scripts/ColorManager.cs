﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorManager : MonoBehaviour {
    static public ColorManager Instance;
    public Color[] listColor;
    private void Awake()
    {
        if (!Instance)
            Instance = this;
    }
}
