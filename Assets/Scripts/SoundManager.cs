﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    static public SoundManager Instance;
    public AudioClip soundBackground;
    public AudioClip soundScore;
    public AudioClip soundGameOver;
    private AudioSource audioSource;

    private void Awake()
    {
        if (!Instance)
            Instance = this;
        audioSource = GetComponent<AudioSource>();
    }
    public void PlaySound(AudioClip audioClip)
    {
        audioSource.clip = audioClip;
        audioSource.Play();
    }

	
}
