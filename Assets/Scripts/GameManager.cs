﻿using UnityEngine;

enum GameState
{
    PrePlay,
    Playing,
    Pause,
    GameOver
}
public class GameManager : MonoBehaviour
{
    static public GameManager Instance;
    private GameState gameState;
    private AudioSource audioSource;

    internal GameState GameState
    {
        get
        {
            return gameState;
        }

        set
        {
            gameState = value;
        }
    }
    private void Awake()
    {
        if (!Instance)
            Instance = this;
        GameState = GameState.Playing;
        audioSource = GetComponent<AudioSource>();
    }
    public void StopMusic()
    {
        audioSource.Stop();
    }

    // Use this for initialization
}
