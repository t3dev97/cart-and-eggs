﻿using UnityEngine;

public class CartController : MonoBehaviour
{
    static public event System.Action PlayerDied;
    public float speedMove;
    private Vector3 posMouse;
    [SerializeField]
    private float halftWidthScreen;
    private float halfWidthCart;
    private Vector2 posStartMouse;
    private bool allowControl = false;
    public Color colorDefaul;
    public Color colorChose;
    // Use this for initialization
    void Start()
    {
        Vector3 temp = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height));
        halftWidthScreen = temp.x;
        halfWidthCart = GetComponent<Renderer>().bounds.size.x / 2;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.GameState == GameState.Playing)
            ControllMove();
    }
    void ControllMove()
    {
        if (Input.GetMouseButtonUp(0))
        {
            allowControl = false;
            transform.GetComponent<Renderer>().material.color = colorDefaul;
        }

        if (Input.GetMouseButtonDown(0))
        {
            //Get the mouse position on the screen and send a raycast into the game world from that position.
            Vector2 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(worldPoint, Vector2.zero);

            //If something was hit, the RaycastHit2D.collider will not be null.
            if (hit.collider != null)
            {
                if (hit.collider.tag == "Player")
                {
                    allowControl = true;
                    hit.collider.GetComponent<Renderer>().material.color = colorChose;
                }
            }
        }

        if (allowControl)
        {
            posMouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            posMouse.y = transform.position.y;
            posMouse.z = transform.position.z;
            posMouse.x = Mathf.Clamp(posMouse.x, -halftWidthScreen + halfWidthCart, halftWidthScreen - halfWidthCart);
            transform.position = Vector3.Lerp(transform.position, posMouse, speedMove * Time.deltaTime);
        }
    }
    private void OnCollisionEnter2D(Collision2D obj)
    {
        if (obj.transform.tag == "Item")
        {
            Destroy(obj.gameObject);
            ScoreManager.Instance.AddScore(1);
        }
        else
        {
            if (obj.transform.tag == "Obstancle")
            {
                //Application.LoadLevel(0);
                PlayerDied();
            }
        }
    }
}
